import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        list.add(1);
        list.add(39);
        list.add(3);
        list.add(28);
        list.add(5);
        list.add(6);
        list.add(7);
        System.out.println(list);
        List<Integer>newList1=list.stream().filter(integer -> integer%2==0).collect(Collectors.toList());
        System.out.println(newList1);
        List<Integer>newList2=list.stream().filter(integer -> integer>2).collect(Collectors.toList());
        System.out.println(newList2);
        List<String> nameList=List.of("Pranjal","Piyush","Vibhay","Sumit","Ravi");
        List<String>newNames=nameList.stream().filter(e->e.startsWith("V")).collect(Collectors.toList());
        System.out.println(newNames);
        List<Integer>newList3=list.stream().map(integer -> integer*integer).collect(Collectors.toList());
        System.out.println(newList3);
        /*nameList.stream().forEach(e->{
            System.out.println(e);
        });*/
        nameList.stream().forEach(System.out::println);
        list.stream().sorted().forEach(System.out::println);
        System.out.println("Maximum");
        Integer integer=list.stream().max((x,y)->x.compareTo(y)).get();
        System.out.println(integer);





    }
}