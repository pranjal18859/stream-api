import java.util.Arrays;
import java.util.stream.Stream;

public class StreamObject {
    public static void main(String[] args) {
        // stream api- collection process
        // group of objects
        //1. Blank stream
        Stream<Object> emptyStream=Stream.empty();
        String name[]={"Pranjal","Piyush","Satyam","Sumit","Ravi"};
        Stream<String> stringStream=Stream.of(name);
        stringStream.forEach(e->{
            System.out.println(e);
        });





    }
}
